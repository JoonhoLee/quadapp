//
// Created by joonho on 2/23/18.
//
#include "quadrotor/QuadrotorControl_vel.hpp"
#include "rai/function/tensorflow/StochasticPolicy_TensorFlow.hpp"
#include "rai/function/tensorflow/RecurrentStochasticPolicyValue_TensorFlow.hpp"
using namespace std;

/// learning states
using Dtype = double;
using rai::Task::ActionDim;
using rai::Task::StateDim;
using rai::Task::CommandDim;
using Policy_TensorFlow = rai::FuncApprox::StochasticPolicy_TensorFlow<Dtype, StateDim, ActionDim>;
using RecurrentPolicy_TensorFlow = rai::FuncApprox::RecurrentStochasticPolicyValue_Tensorflow<Dtype, StateDim, ActionDim>;

using Task = rai::Task::QuadrotorControl<Dtype>;
int test(){
  cout << "test";
}

int main(){
  RAI_init();
  Policy_TensorFlow policy("cpu", "MLP", "relu 3e-3 21 128 128 4", 1e-3);
  policy.loadParam("/home/jalim/Documents/trponoise3.txt");

  Task task;
  Task::Action action;
  Task::State state;
  rai::Tensor<Dtype,2> action_T("action");
  rai::Tensor<Dtype,2> state_T("state");
  rai::TerminationType termt;
  Dtype cost;

  ///Configure task
  /// trajectoryID 0 : Stationary 1 : Circular 2: Lemniscate
  int trajectoryID = 1;
  task.enableSimulation();
  task.turnOnVisualization("");
  task.startRecordingVideo(RAI_LOG_PATH, "tt");

  ///Initialize Task

  state_T.resize({StateDim, 1});
  action_T.resize({ActionDim, 1});
  Eigen::Vector3d targetacc, targetvel, init_targetpos, offset_targetpos, targetpos, quadpos, quadvel;
  Eigen::Matrix<double, 3, 3> rotmat;
  Eigen::Matrix<double, 4, 1> quat;

  ///Circular trajectory
  double traj_radius = 2.0;
  Eigen::Vector3d traj_axis;
  traj_axis << 0.0, 0.0, 1.0;
  init_targetpos << 0.0, traj_radius, 0.0;
  offset_targetpos << 0.0, 0.0, 1.0;
  StopWatch watch;

  task.setTrajectory(trajectoryID);
  task.setInitialDistance(0.0);

  Eigen::Matrix<double, 3, 1> positionError;
  double cumsum;
  double RMSE, RMSEsum = 0.0;

  int timeStepsPerEpisode = 2000;
  int numTrials = 5;
  for (int trial = 0; trial < numTrials; trial++) {
    task.init();
    task.getState(state);
    cumsum = 0.0;
    for (int i = 0; i < timeStepsPerEpisode; i++) {

      state_T.eMat() = state;
      policy.forward(state_T, action_T);
      action = action_T.eMat();
      task.step(action, state, termt, cost);

      positionError = state.segment<3>(9);
      cumsum += std::pow(positionError.norm(), 2);

      double waitTime = std::max(0.0, 0.01 - watch.measure("sim", true));
      watch.start("sim");
      usleep(waitTime * 1e6);
//      if (termt != rai::TerminationType::not_terminated) {
//        std::cout << "failed" << std::endl;
//        i = timeStepsPerEpisode;
//      }
    }

    RMSE = std::sqrt(cumsum / timeStepsPerEpisode);
    RMSEsum += RMSE;
    std::cout << "RMSE : " << RMSE << std::endl;
  }
  std::cout << "mean RMSE : " << RMSEsum/numTrials << std::endl;

  task.endRecordingVideo();
  do
  { cout << '\n' << "wait for video saving message and press enter";
  } while (cin.get() != '\n');
  return 0;
};