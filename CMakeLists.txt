cmake_minimum_required(VERSION 3.5)
project(APP)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_COMPILER "/usr/bin/g++-7")
set(CMAKE_CXX_FLAGS "-Wl,--no-as-needed -fopenmp -O3 -w -funroll-loops")

find_package(RAI REQUIRED)
include_directories(${TENSORFLOW_EIGEN_DIR})
include_directories(${RAI_INCLUDE_DIR})

# Search for raiGraphics
find_package(raiCommon CONFIG REQUIRED)
include_directories(${RAI_COMMON_INCLUDE_DIR})

# Search for raiGraphics
find_package(raiGraphicsOpengl CONFIG REQUIRED)
include_directories(${RAI_GRAPHICS_OPENGL_INCLUDE_DIRS})

include_directories(task/include)

add_executable(quadrotorRNN quadrotorRNN.cpp
        task/src/quadrotor/visualizer/Quadrotor_Visualizer3.cpp)
target_link_libraries(quadrotorRNN ${RAI_LINK} ${RAI_GRAPHICS_OPENGL_LINK} ${RAI_COMMON_LINK})

add_executable(quadrotor_TRPO
        task/src/quadrotor/visualizer/Quadrotor_Visualizer3.cpp
        quadrotor_TRPO.cpp)
target_link_libraries(quadrotor_TRPO ${RAI_LINK} ${RAI_GRAPHICS_OPENGL_LINK} ${RAI_COMMON_LINK})

add_executable(quadrotor_PPO
        task/src/quadrotor/visualizer/Quadrotor_Visualizer3.cpp
        quadrotor_PPO.cpp)
target_link_libraries(quadrotor_PPO ${RAI_LINK} ${RAI_GRAPHICS_OPENGL_LINK} ${RAI_COMMON_LINK})

add_executable(quadsim
        task/src/quadrotor/visualizer/Quadrotor_Visualizer3.cpp
        quad_simul.cpp)
target_link_libraries(quadsim ${RAI_LINK} ${RAI_GRAPHICS_OPENGL_LINK} ${RAI_COMMON_LINK})
