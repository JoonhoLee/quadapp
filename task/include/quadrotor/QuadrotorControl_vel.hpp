//
// Created by jhwangbo on 30.11.16.
//

//#ifndef RAI_QUADROTORCONTROL_HPP
//#define RAI_QUADROTORCONTROL_HPP

// custom inclusion- Modify for your task
#include "rai/tasks/common/Task.hpp"
#include "raiCommon/enumeration.hpp"
#include "raiCommon/utils/RandomNumberGenerator.hpp"
#include "raiCommon/TypeDef.hpp"
#include "raiCommon/math/inverseUsingCholesky.hpp"
#include "raiCommon/math/RAI_math.hpp"
#include "raiGraphics/RAI_graphics.hpp"
#include "quadrotor/visualizer/Quadrotor_Visualizer3.hpp"
#include "raiCommon/utils/StopWatch.hpp"
#include "rai/noiseModel/OrnsteinUhlenbeckNoise.hpp"

#pragma once

namespace rai {
namespace Task {

constexpr int StateDim = 21; // last 3: targetvel
constexpr int ActionDim = 4;
constexpr int CommandDim = 0;

template<typename Dtype>
class QuadrotorControl : public Task<Dtype,
                                     StateDim,
                                     ActionDim,
                                     CommandDim> {
 public:
  using TaskBase = Task<Dtype, StateDim, ActionDim, CommandDim>;
  using State = typename TaskBase::State;
  using StateBatch = typename TaskBase::StateBatch;
  using Action = typename TaskBase::Action;
  using ActionBatch = typename TaskBase::ActionBatch;
  using Command = typename TaskBase::Command;
  using MatrixXD = Eigen::Matrix<Dtype, Eigen::Dynamic, Eigen::Dynamic>;
  using VectorXD = Eigen::Matrix<Dtype, Eigen::Dynamic, 1>;
  using PhysicalState = VectorXD;
  using MatrixJacobian = typename TaskBase::JacobianStateResAct;
  using MatrixJacobianCostResAct = typename TaskBase::JacobianCostResAct;

  using GeneralizedCoordinate = Eigen::Matrix<double, 7, 1>;
  using GeneralizedVelocity = Eigen::Matrix<double, 6, 1>;
  using GeneralizedAcceleration = Eigen::Matrix<double, 6, 1>;

  QuadrotorControl() : ornNoise_(0.1, 0.15) {


    //// set default parameters
    this->valueAtTermination_ = 1.5;
    this->discountFactor_ = 0.99;
    this->timeLimit_ = 15.0;
    this->controlUpdate_dt_ = 0.01;
    gravity_ << 0.0, 0.0, -9.81;

    Eigen::Vector3d diagonalInertia;
//    diagonalInertia << 0.0105, 0.0105, 0.018;
    diagonalInertia << 0.007, 0.007, 0.012;

    inertia_ = diagonalInertia.asDiagonal();
    cholInv(inertia_, inertiaInv_);
    comLocation_ << 0.0, 0.0, -0.05;

    //////// Coefficients for Aerodynamic effects /////
    Eigen::Vector3d diagonalDragcoeff, e3;
    Eigen::Matrix3d pi_e3, e3_x;
    e3 << 0.0, 0.0, 1.0;
    e3_x << 0.0, -1.0, 0.0,
        1.0, 0.0, 0.0,
        0.0, 0.0, 0.0;
    pi_e3.setIdentity();
    pi_e3 = pi_e3 - e3 * e3.transpose();

    //Coefficients
    k_h_ =
        0.0; // Thrust variation effects from horizontal velocity From Svacha, Kuumar(2017) //TODO: Should be randomized
    c12_ = 0.0;
    d11_ = 0.0;
    d33_ = 0.0;
//    diagonalDragcoeff << 0.544, 0.386, 0.00; // From Faessler(2018), circular trajectory
    diagonalDragcoeff << 0.491, 0.236, 0.00; // From Faessler(2018), lemniscate trajectory

    D_ = diagonalDragcoeff.asDiagonal();
    A_ = -c12_ * e3_x;
    B_ = d11_ * pi_e3 + d33_ * e3 * e3.transpose();

    /////// scale //////
    actionScale_.setConstant(2.0);
    orientationScale_ = 1.0;
//    positionScale_ = 5.0;
    positionScale_ = 0.2;

    angVelScale_ = 0.15;
    linVelScale_ = 0.25;

    /////// adding constraints////////////////////
    State upperStateBound, lowerStateBound;
    upperStateBound << 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0,
        3.0, 3.0, 3.0,
        5.0, 5.0, 5.0,
        6.0, 6.0, 6.0,
        10.0, 10.0, 10.0;
    lowerStateBound = -upperStateBound;
    this->setBoxConstraints(lowerStateBound, upperStateBound);
    transsThrust2GenForce << 0, 0, length_, -length_,
        -length_, length_, 0, 0,
        dragCoeff_, dragCoeff_, -dragCoeff_, -dragCoeff_,
        1, 1, 1, 1;
    transsThrust2GenForceInv = transsThrust2GenForce.inverse();
    targetPosition.setZero();

    ////// Task spec ////
    randomize_ = false;
    counter = 0;
    initialDist_ = 0.5;
    maxDist_ = 1;
    noiseFactor_ = 0.2;
    theta_ = 0.0;
    target_trajectoryID_ = 0;

  }

  ~QuadrotorControl() {
  }

  void randomizeDynamics() {
    if (randomize_) {
      actionScale_ << 2 + noiseFactor_ * rn_.sampleUniform(), 2 + noiseFactor_ * rn_.sampleUniform(), 2
          + noiseFactor_ * rn_.sampleUniform(),
          2 + noiseFactor_ * rn_.sampleUniform();
    }
  }

  void setRandomize(bool randomize = true) {
    randomize_ = randomize;
  }

  void setNoiseFactor(const double in) {
    noiseFactor_ = in;
  }

  void step(const Action &action_t,
            State &state_tp1,
            TerminationType &termType,
            Dtype &costOUT) {
    //Get current state from q_, u_
    orientation = q_.head(4); //Orientation of quadrotor
    double angle = 2.0 * std::acos(q_(0)); //Angle of rotation

    position = q_.tail(3); //Position of quadrotor
    LinearVelocity quadvel = u_.tail(3);

    R_ = Math::MathFunc::quatToRotMat(orientation); //Calculate rotation matrix
    v_I_ = u_.tail(3); //linear velocity
    w_I_ = u_.head(3); //angular velocity
    v_B_ = R_.transpose() * u_.tail(3); //body linear velocity inertial
    w_B_ = R_.transpose() * w_I_; //body rates

    //Control input from action
    Action actionGenForce;
    actionGenForce = transsThrust2GenForce * action_t.cwiseProduct(actionScale_); //Force generated by action
    B_torque = actionGenForce.segment(0, 3); //Moment input
    // Thrust variation effects from horizontal velocity From Svacha, Kuumar(2017)
    actionGenForce(3) = actionGenForce(3) + k_h_ * (v_B_.head(2)).transpose() * v_B_.head(2);
    B_force << 0.0, 0.0, actionGenForce(3); // Thrust vector in {B}

    {
      //Control input from PD stabilization
      double kp_rot = -0.2, kd_rot = -0.06;
      Torque fbTorque_b;

      if (angle > 1e-6)
        fbTorque_b = kp_rot * angle * (R_.transpose() * orientation.tail(3))
            / std::sin(angle) + kd_rot * (R_.transpose() * u_.head(3));
      else
        fbTorque_b = kd_rot * (R_.transpose() * u_.head(3));
      fbTorque_b(2) = fbTorque_b(2) * 0.15; //Lower yaw gains
      B_torque += fbTorque_b; //Sum of torque inputs
    }

    B_force(2) += mass_ * 9.81; //Sum of thrust inputs

    // clip inputs
    Action genForce;
    genForce << B_torque, B_force(2);
    Action thrust = transsThrust2GenForceInv * genForce;    // TODO: This is not exactly thrust: change name
    thrust = thrust.array().cwiseMax(1e-8); // clip for min value
    thrust = thrust.array().cwiseMin(7.0); // clip for max value

    genForce = transsThrust2GenForce * thrust;
    B_torque = genForce.segment(0, 3);
    B_force(2) = genForce(3);

    //// Calculate Aerodynamic effects ////
    Eigen::Vector3d a_drag, tau_g, t_drag;
    a_drag = R_ * D_ * R_.transpose() * u_.tail(3); // Calculate First order Drag Refernce: Faessler(2018)
    tau_g << 0.0, 0.0, 0.0; //Gyroscopic effects: Assuming rigid rotors, this is negligible for flexible rotors
//    tau_g = w_B_.cross(prop_inertia_ * prop_w_); //Gyroscopic effects
    // Assuming rigid rotors, this is negligible for flexible rotors

    //t_drag = tau_g + A_ * R_.transpose() * u_.tail(3) - B_ * u_.head(3);  //TODO: Calculation of A, B from Kai(2017)

    du_.tail(3) = (R_ * B_force) / mass_ + gravity_ - a_drag; // quad acceleration
    du_.head(3) = R_ * (inertiaInv_ * (B_torque - w_B_.cross(inertia_ * w_B_))); //acceleration by inputs

    Math::MathFunc::normalizeQuat(orientation);

    //Integrate
    moveTarget();
    u_ += du_ * this->controlUpdate_dt_; //velocity after timestep
    w_IXdt_ = w_I_ * this->controlUpdate_dt_;
    w_I_ = u_.head(3);
    orientation = Math::MathFunc::boxplusI_Frame(orientation, w_IXdt_);
    q_.head(4) = orientation;
    q_.tail(3) = q_.tail(3) + u_.tail(3) * this->controlUpdate_dt_;
    w_B_ = R_.transpose() * u_.head(3);

    // visualizer_.drawWorld(visualizeFrame, position, orientation);

    if (std::isnan(orientation.norm())) {
      std::cout << "u_ " << u_.transpose() << std::endl;
      std::cout << "du_ " << du_.transpose() << std::endl;
      std::cout << "B_torque " << B_torque.transpose() << std::endl;
      std::cout << "orientation " << orientation.transpose() << std::endl;
      std::cout << "state_tp1 " << q_.transpose() << std::endl;
      std::cout << "action_t " << action_t.transpose() << std::endl;
    }

    u_(0) = clip(u_(0), -20.0, 20.0);
    u_(1) = clip(u_(1), -20.0, 20.0);
    u_(2) = clip(u_(2), -20.0, 20.0);
    u_(3) = clip(u_(3), -5.0, 5.0);
    u_(4) = clip(u_(4), -5.0, 5.0);
    u_(5) = clip(u_(5), -5.0, 5.0);

    getState(state_tp1);

    counter++;
//
//    costOUT = 0.005 * std::sqrt((q_.tail(3) - p_targ).norm())+
//        0.001 *  std::sqrt((u_.tail(3) - v_targ).norm()) +
//        0.0001 * action_t.norm() +
//        0.0001 * u_.head(3).norm();

    costOUT = 0.005 * std::sqrt(std::min((q_.tail(3) - p_targ).norm(), 1.5))+
//        0.001 *  std::sqrt((u_.tail(3) - v_targ).norm()) +
        0.0001 * action_t.norm() +
        0.0001 * std::min( u_.head(3).norm(), 1.0);


    // visualization
    if (this->visualization_ON_) {
      visualizer_.drawWorld(position, orientation, quadvel, p_targ, v_targ);

//      (const rai::Position &quadPos, const rai::Quaternion &quadAtt, const rai::Position &quadvel,  const rai::Position &targetPos, const rai::Position &targetvel)
      double waitTime = std::max(0.0, this->controlUpdate_dt_ / realTimeRatio - watch.measure("sim", true));
      watch.start("sim");
      usleep(waitTime * 1e6);
    }
  }

  void changeTarget(Position &position) {
    targetPosition = position;
  }

  bool isTerminalState(State &state) {
    if ((q_.tail(3) - p_targ).norm() > 5) return true;
    return false;
  }

  void init() {
    /// initial state is random
    double oriF[4], posiF[3], angVelF[3], linVelF[3], targVel[3];
    rn_.template sampleOnUnitSphere<4>(oriF);
    rn_.template sampleVectorInNormalUniform<3>(posiF);
    rn_.template sampleVectorInNormalUniform<3>(angVelF);
    rn_.template sampleInUnitSphere<3>(linVelF);
    rn_.template sampleInUnitSphere<3>(targVel);

    Quaternion orientation;
    Position position;
    AngularVelocity angularVelocity;
    LinearVelocity linearVelocity;

    linearVelocity << double(linVelF[0]), double(linVelF[1]), double(linVelF[2]);
    counter = 0;

    ///target
    p_targ << double(posiF[0]), double(posiF[1]), double(posiF[2]);
    v_targ << double(targVel[0]), double(targVel[1]), double(targVel[2]);
    a_targ << 0, 0, 0;
    xVel_ = 7.0 * rn_.sampleUniform01();

    ///quad
    rn_.template sampleVectorInNormalUniform<3>(posiF);
    position << double(posiF[0]) * 1., double(posiF[1]) * 1., double(posiF[2]) * 1.;
    position = p_targ + (initialDist_ * position);

    orientation << double(std::abs(oriF[0])), double(oriF[1]), double(oriF[2]), double(oriF[3]);
    rai::Math::MathFunc::normalizeQuat(orientation);
    angularVelocity << double(angVelF[0]), double(angVelF[1]), double(angVelF[2]);

    if (!training_ON_) {
      position = target_initpos + (initialDist_ * position);
      angularVelocity.setZero();
      orientation << 1.0, 0.0, 0.0, 0.0;
      linearVelocity.setZero();
    }
    
    q_ << orientation, position;
    u_ << angularVelocity, linearVelocity;

    ///traj
    theta_ = 0.0;

    randomizeDynamics();
  }

  void initTo(const State &state) {
    State stateT = state;
    double posiF[3];
    rn_.template sampleVectorInNormalUniform<3>(posiF);
    p_targ << double(posiF[0]), double(posiF[1]), double(posiF[2]);
    R_.col(0) = stateT.segment(0, 3);
    R_.col(1) = stateT.segment(3, 3);
    R_.col(2) = stateT.segment(6, 3);
    orientation = Math::MathFunc::rotMatToQuat(R_);
    q_.head(4) = orientation;
    q_.tail(3) = state.segment(9, 3) + p_targ / positionScale_;
    u_.head(3) = state.segment(12, 3) / angVelScale_;
    u_.tail(3) = state.segment(15, 3) / linVelScale_;
    counter = 0;
    xVel_ = 7 * rn_.sampleUniform01();

    ///target
    theta_ = 0.0;

    randomizeDynamics();
  }
  void setInitialDistance(const double dist) {
    initialDist_ = std::min(dist, maxDist_);
  }

  void setMaxInitialDistance(const double dist) {
    maxDist_ = dist;
  }

  void setTrajectory(int ID = 0) {
    double radius = 0, omega = 0;
    Eigen::Vector3d axis, initpos;

    switch (ID) {
      case 0: //stationary trajectory
        omega = 0.0;
        radius = 2.0;
        axis << 0.0, 0.0, 1.0;
        initpos << 0.0, radius, 0.0;
        break;
      case 1: //circular trajectory
        omega = 1.0;
        radius = 2.0;
        axis << 0.0, 0.0, 1.0;
        initpos << 0.0, radius, 0.0;
        break;
      case 2: //Lemniscate of Genoro
        omega = 1.0;
        radius = 2.0;
        axis << 0.0, 0.0, 1.0;
        initpos << 0.0, radius, 0.0;
        break;
    }
    setTrajectory(ID, omega, axis, radius, initpos);
  }

  void setTrajectory(int ID, double omega, Eigen::Vector3d axis, double radius, Eigen::Vector3d initpos) {
    target_trajectoryID_ = ID;
    traj_axis_ = axis;
    traj_omega_ = omega;
    traj_radius_ = radius;
    target_initpos = initpos;
  }

  void setTrajectoryTheta(double in) {
    theta_ = in;
  }

  void moveTarget() {
    if (training_ON_) {
      // Bounded Jerk and Acceleration values
      // Refernece: Hehn(2011) Maximum Jerk: 50 m/s^3 Maximum Acceleration 7m/s^2

      Eigen::Vector3d a_targ_new, a_tangent_new, v_targ_new;
      xVel_ = std::min(7.0, xVel_ + 0.1 * rn_.sampleUniform());

      j_targ = 50 * ornNoise_.sampleNoise();
      if (j_targ.norm() > 50)
        j_targ = j_targ / j_targ.norm() * 50;
      a_targ_new = a_targ + j_targ * this->controlUpdate_dt_;

      while (true) {
        j_targ = 50 * ornNoise_.sampleNoise();
        if (j_targ.norm() > 50)
          j_targ = j_targ / j_targ.norm() * 50;
        a_targ_new = a_targ + j_targ * this->controlUpdate_dt_;
        v_targ_new = v_targ + a_targ_new * this->controlUpdate_dt_;

        ///check velocity limit & rescale
        if (v_targ_new.norm() > xVel_) {
          v_targ_new = v_targ_new / v_targ_new.norm() * xVel_;
          a_tangent_new = (v_targ_new - v_targ) / this->controlUpdate_dt_;
        }

        ///check acceleration limit
//        if (a_targ_new.norm() <= 7) break;
        if (a_targ_new.norm() <= 10) break;
      }

      v_targ = v_targ_new;
      a_targ = a_targ_new;
      p_targ += v_targ * this->controlUpdate_dt_;
    } else {
      if (target_trajectoryID_ == 0) { //Stationary
        p_targ = target_initpos;
        v_targ.setZero();
      } else if (target_trajectoryID_ == 1) { //Circular trajectory
        p_targ = std::cos(theta_) * target_initpos
            + std::sin(theta_) * traj_axis_.cross(target_initpos)
            + (1 - std::cos(theta_)) * traj_axis_.dot(target_initpos) * traj_axis_;
        v_targ = traj_omega_ * traj_axis_.cross(p_targ);
        theta_ += traj_omega_ * this->controlUpdate_dt_;
      } else if (target_trajectoryID_ == 2) { //Lemniscate of Genero
        p_targ = std::cos(theta_) * target_initpos
            + std::sin(theta_) * std::cos(theta_) * traj_axis_.cross(target_initpos)
            + (1 - std::cos(theta_)) * traj_axis_.dot(target_initpos) * traj_axis_;
        v_targ = traj_omega_ * traj_axis_.cross(p_targ);
        theta_ += traj_omega_ * this->controlUpdate_dt_;

      }
    }

  }

  void setTargetVelocity(LinearVelocity &target) {
    v_targ = target;
  }

  void translate(Position &position) {
    q_.segment(4, 3) += position;
  }

  void getInitialState(State &state) {
    init();
    getState(state);
  }

  void setInitialState(const State &in) {
    //LOG(FATAL) << "The initial state is random. No need to set it" << std::endl;
  }

  void getState(State &state) {
    //LOG_IF(FATAL, std::isnan(q_.head(4).norm())) << "simulation unstable";
    orientation = q_.head(4);
    Math::MathFunc::normalizeQuat(orientation);
    R_ = Math::MathFunc::quatToRotMat(orientation);
    state << R_.col(0), R_.col(1), R_.col(2),
        (q_.tail(3) - p_targ) * positionScale_,
        u_.head(3) * angVelScale_,
        u_.tail(3) * linVelScale_, v_targ * linVelScale_;
  };

  void getOrientation(Quaternion &quat) {
    quat = orientation;
  }

  void getPosition(Position &posi) {
    posi = q_.tail(3);
  }

  void getLinvel(LinearVelocity &linvel) {
    linvel = u_.tail(3);
  }

  void getAngvel(AngularVelocity &angvel) {
    angvel = u_.head(3);
  }

  void startRecordingVideo(std::string dir, std::string fileName) {
    mkdir(dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    visualizer_.getGraphics()->savingSnapshots(dir, fileName);
  }

  void endRecordingVideo() {
    visualizer_.getGraphics()->images2Video();
  }

  void enableSimulation() {
    training_ON_ = false;
  }

  void enableTraining() {
    training_ON_ = true;
  }

 private:

  double clip(double input, double lower, double upper) {
    input = (input > lower) * input + !(input > lower) * lower;
    return (input < upper) * input + !(input < upper) * upper;
  }

  template<typename T>
  inline double sgn(T val) {
    return double((T(0) < val) - (val < T(0)));
  }

  ///general
  RandomNumberGenerator<Dtype> rn_;

  ///learning params
 public:
  double orientationScale_, positionScale_, angVelScale_, linVelScale_;

 private:
  double initialDist_, maxDist_;
  double noiseFactor_;
  Action actionScale_;
  bool randomize_;

  /// robot parameters
  GeneralizedCoordinate q_; // generalized state and velocity
  GeneralizedVelocity u_;
  GeneralizedAcceleration du_;
  RotationMatrix R_;
  Eigen::Matrix4d transsThrust2GenForce;
  Eigen::Matrix4d transsThrust2GenForceInv;
  Inertia inertia_, prop_inertia_;
  Inertia inertiaInv_;
  LinearAcceleration gravity_;
  Position comLocation_;
  Position position;
  Quaternion orientation;
  Force B_force;
  Torque B_torque;
  AngularVelocity w_I_, w_B_;
  LinearVelocity v_I_, v_B_;
  double length_ = 0.17;
  double dragCoeff_ = 0.016;
  double mass_ = 0.665;
  EulerVector w_IXdt_;

  ///Target
  rai::Noise::OrnsteinUhlenbeck<double, 3> ornNoise_;
  Eigen::Vector3d v_targ, cmd_a_targ, a_targ, p_targ, j_targ, a_drag;
  double xVel_;
  int counter;
  Eigen::Vector3d traj_axis_;
  double traj_radius_, traj_omega_;
  double theta_ = 0.0;

  ///For drag coefficients
  double k_h_, d11_, d33_, c12_;
  Eigen::Matrix3d A_, B_, D_; // Normalized drag coefficient matrix;
  int target_trajectoryID_;
  Eigen::Vector3d target_initpos;
  bool training_ON_ = true;

  //Visualization
  StopWatch watch;
  double realTimeRatio = 1;
  static Quadrotor_Visualizer3 visualizer_;
  HomogeneousTransform visualizeFrame;
  static rai_graphics::RAI_graphics graphics;
  //  static rai_graphics::object::Quadrotor quadrotor;
  static rai_graphics::object::Sphere target;
  static Position targetPosition;
  double visualizationTime = 0;
};
}
} /// namespaces
template<typename Dtype>
rai::Position rai::Task::QuadrotorControl<Dtype>::targetPosition;
template<typename Dtype>
Quadrotor_Visualizer3 rai::Task::QuadrotorControl<Dtype>::visualizer_;
//#endif //RAI_QUADROTORCONTROL_HPP