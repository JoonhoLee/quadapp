
#include "raiCommon/math/RAI_math.hpp"
#include "raiGraphics/RAI_graphics.hpp"
#include "raiGraphics/obj/Mesh.hpp"
#include "raiGraphics/obj/Cylinder.hpp"
#include "raiGraphics/obj/Sphere.hpp"
#include "raiGraphics/obj/Quadrotor.hpp"


class Quadrotor_Visualizer3 {

 public:
  using GeneralizedCoordinate = Eigen::Matrix<double, 7, 1>;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Quadrotor_Visualizer3();

  ~Quadrotor_Visualizer3();

  void setTerrain(std::string fileName);
  void drawWorld(const rai::Position &quadPos,const rai::Quaternion &quadAtt, const rai::Position &quadvel,const  rai::Position &targetvel);
  void drawWorld(const rai::Position &quadPos, const rai::Quaternion &quadAtt, const rai::Position &quadvel,  const rai::Position &targetPos, const rai::Position &targetvel);
    void reinitialize();
  rai_graphics::RAI_graphics* getGraphics();

private:
  rai_graphics::RAI_graphics graphics;
  rai_graphics::object::Quadrotor quadrotor;
  rai_graphics::object::Sphere Target;
  rai_graphics::object::Sphere Center;
  rai_graphics::object::Arrow targetArrow;
  rai_graphics::object::Arrow arrow;

  rai_graphics::object::Background background;
  rai_graphics::object::CheckerBoard checkerboard;

  rai::HomogeneousTransform defaultPose_;
  double offset;
};
