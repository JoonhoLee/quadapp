#include <quadrotor/visualizer/Quadrotor_Visualizer3.hpp>

Quadrotor_Visualizer3::Quadrotor_Visualizer3() :
    graphics(600, 400),
    quadrotor(0.3),
    Target(0.025),
    Center(0.025),
    background("sky"),checkerboard(1, 100, 100, 0,{0.0f,0.0f,0.0f},{1.0f,1.0f,1.0f}), arrow(0.1f,0.2f,1.0f,0.5f), targetArrow(0.1f,0.2f,1.0f,0.5f){
  offset = 5.0;

  checkerboard.gridMode=true;

  Target.setColor({1.0f, 0.0f, 0.0f});
  targetArrow.setColor({1.0f, 0.0f, 0.0f});
  arrow.setColor({0.0f, 1.0f, 0.0f});
  targetArrow.setTransparency(0.5f);
  arrow.setTransparency(0.5f);
  Center.setColor({0.0f, 0.0f, 1.0f});

  defaultPose_.setIdentity();
  rai::Math::MathFunc::rotateHTabout_x_axis(defaultPose_, -M_PI_2);

  graphics.addSuperObject(&quadrotor);
  graphics.addObject(&Target);
  graphics.addObject(&targetArrow);
  graphics.addObject(&arrow);
  graphics.addObject(&Center);


//  graphics.addBackground(&background);
  graphics.setBackgroundColor(1.0f, 1.0f, 1.0f, 1.0f);
  graphics.addCheckerBoard(&checkerboard);

  Eigen::Vector3d relPos;
  relPos << -5.0, 0.0, 1.0;
  std::vector<float> pos = {-100.0f, 0.0f, 0.0f}, spec = {0.7f, 0.7f, 0.7f}, amb = {0.7f, 0.7f, 0.7f}, diff = {0.7f, 0.7f, 0.7f};


  rai_graphics::LightProp lprop;
  lprop.amb_light = amb;
  lprop.spec_light = spec;
  lprop.diff_light = diff;
  lprop.pos_light = pos;
  rai_graphics::CameraProp cprop;
//  cprop.toFollow = quadrotor.basePtr();
//  cprop.toFollow = &Center;
  cprop.toFollow = &Target;
  cprop.relativeDist = relPos;

  graphics.setCameraProp(cprop);
  graphics.setLightProp(lprop);
  graphics.start();

}

Quadrotor_Visualizer3::~Quadrotor_Visualizer3() {
  graphics.end();
}

void Quadrotor_Visualizer3::drawWorld(const rai::Position &quadPos, const rai::Quaternion &quadAtt, const rai::Position &quadvel, const rai::Position &targetvel) {

}

void Quadrotor_Visualizer3::drawWorld(const rai::Position &quadPos, const rai::Quaternion &quadAtt, const rai::Position &quadvel,  const rai::Position &targetPos, const rai::Position &targetvel) {
  Eigen::Vector3d pos;
  Eigen::Vector3d targetpos, refpos;
  targetpos = targetPos;
  targetpos[2] += offset;
  refpos << 0.0, 0.0, 0.0;
  refpos[2] += offset;

  rai::HomogeneousTransform quadPose;
  rai::Quaternion quadOri = quadAtt;

  quadPose.setIdentity();
  quadPose.topRightCorner(3, 1) = quadPos;
  quadPose(2,3) += offset;
  quadPose.topLeftCorner(3,3) = rai::Math::MathFunc::quatToRotMat(quadOri);
  quadPose = quadPose * defaultPose_;

//State
  Eigen::Vector3d xVector_;
  xVector_ << 1.0,0.0,0.0;
  Eigen::Vector3d tempVel = quadvel;
  Eigen::Vector3d tempVel2;

  double Mag = tempVel.norm();

  tempVel.normalize();
  tempVel2 = xVector_.cross(tempVel);
  tempVel2.normalize();
  double Ang = std::acos(xVector_.dot(tempVel));

  rai::Quaternion orientation = rai::Math::MathFunc::angleAxisToQuat(Ang, tempVel2);

  quadrotor.setPose(quadPose);
  quadrotor.spinRotors();

  Target.setPos(targetpos);
  Center.setPos(refpos);

  tempVel = quadPose.topRightCorner(3, 1);
  arrow.setPose(tempVel,orientation);
  arrow.setScale(Mag/10.0f);

  tempVel = targetvel;
  Mag = tempVel.norm();

  tempVel.normalize();
  tempVel2 = xVector_.cross(tempVel);
  tempVel2.normalize();
  Ang = std::acos(xVector_.dot(tempVel));
  orientation = rai::Math::MathFunc::angleAxisToQuat(Ang, tempVel2);

  targetArrow.setPose(targetpos, orientation);
  targetArrow.setScale(Mag/10.0f);

}

rai_graphics::RAI_graphics *Quadrotor_Visualizer3::getGraphics() {
  return &graphics;
}