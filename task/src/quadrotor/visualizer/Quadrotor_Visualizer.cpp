
#include <quadrotor/visualizer/Quadrotor_Visualizer.hpp>

Quadrotor_Visualizer::Quadrotor_Visualizer() :
    graphics(600, 450),
    quadrotor(0.3),
    Target(0.025),
    background("sky"),checkerboard(1, 100, 100, 0,{0,0,0},{1,1,1}){
  offset = 5.0;

  checkerboard.gridMode=true;
  Target.setColor({1.0, 0.0, 0.0});

  defaultPose_.setIdentity();
  rai::Math::MathFunc::rotateHTabout_x_axis(defaultPose_, -M_PI_2);

  graphics.addSuperObject(&quadrotor);
  graphics.addObject(&Target);
//  graphics.addBackground(&background);
  graphics.setBackgroundColor(1, 1, 1, 1);
  graphics.addCheckerBoard(&checkerboard);

  Eigen::Vector3d relPos;
  relPos << -2, 0, 0;
  std::vector<float> pos = {-100, 0, 0}, spec = {0.7, 0.7, 0.7}, amb = {0.7, 0.7, 0.7}, diff = {0.7, 0.7, 0.7};

  rai_graphics::LightProp lprop;
  lprop.amb_light = amb;
  lprop.spec_light = spec;
  lprop.diff_light = diff;
  lprop.pos_light = pos;
  rai_graphics::CameraProp cprop;
  cprop.toFollow = quadrotor.basePtr();
  cprop.relativeDist = relPos;

  graphics.setCameraProp(cprop);
  graphics.setLightProp(lprop);
  graphics.start();

}

Quadrotor_Visualizer::~Quadrotor_Visualizer() {
  graphics.end();
}

void Quadrotor_Visualizer::drawWorld(rai::Position &quadPos, rai::Quaternion &quadAtt) {
  Eigen::Vector3d pos;
  rai::HomogeneousTransform quadPose;
  rai::RotationMatrix rotmat;


  quadPose.setIdentity();
  quadPose.topRightCorner(3, 1) = quadPos;
  quadPose(2,3) += offset;
  quadPose.topLeftCorner(3,3) = rai::Math::MathFunc::quatToRotMat(quadAtt);


  quadPose = quadPose * defaultPose_;

  quadrotor.setPose(quadPose);
  quadrotor.spinRotors();

  Target.setPos(0,0,offset);

}

rai_graphics::RAI_graphics *Quadrotor_Visualizer::getGraphics() {
  return &graphics;
}